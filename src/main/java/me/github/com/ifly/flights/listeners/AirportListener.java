package me.github.com.ifly.flights.listeners;

import lombok.AllArgsConstructor;
import lombok.Data;
import me.github.com.ifly.flights.configurations.Channels;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;
import java.util.Map;

@Configuration
@AllArgsConstructor
class AirportListener {

    private CreationAirportService service;

    @Data
    static class CreatedAirportEvent {
        private LocalDateTime time;
        private String identifier;
        private Map<String, Object> location;
    }

    @StreamListener(Channels.AIRPORTS)
    void handle(CreatedAirportEvent event) {
        service.registerAirport(event);
    }

}
