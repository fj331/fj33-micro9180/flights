package me.github.com.ifly.flights.configurations;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.MessageChannel;

public interface Channels {

    String AIRPORTS = "airports";

    @Input(AIRPORTS)
    MessageChannel airports();
}
