package me.github.com.ifly.flights.features;

import me.github.com.ifly.flights.infra.ResourceNotFoundException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "aircraft", fallback = AircraftClient.AircraftClientFallback.class)
public interface AircraftClient {

    @RequestMapping(method = RequestMethod.HEAD, value = "{identifier}")
    void existAircraftBy(@PathVariable String identifier);

    @Component
    class AircraftClientFallback implements AircraftClient {
        @Override
        public void existAircraftBy(String identifier) {
            throw new ResourceNotFoundException("Cannot find aircraft");
        }
    }
}
