package me.github.com.ifly.flights.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;

@Entity
@Table(name = "flights")
@RequiredArgsConstructor
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class Flight {
    @Id
    @Getter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Getter
    private BigDecimal price;

    @NotNull
    @NonNull
    @OneToOne
    @JoinColumn(name = "departure_id")
    private Schedule departure;

    @NotNull
    @NonNull
    @OneToOne
    @JoinColumn(name = "arrival_id")
    private Schedule arrival;

    public LocalDateTime getTime() {
        return departure.getTime();
    }

    public LocalDateTime estimatedArrivalTime() {
        return arrival.getTime();
    }

    public Duration getDuration() {
        return Duration.between(departure.getTime(), arrival.getTime());
    }
}
