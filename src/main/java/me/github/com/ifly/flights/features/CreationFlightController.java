package me.github.com.ifly.flights.features;

import lombok.AllArgsConstructor;
import lombok.Data;
import me.github.com.ifly.flights.domain.Site;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;

import java.time.LocalDateTime;

import static org.springframework.http.ResponseEntity.created;

@RestController
@RequestMapping("flights")
@AllArgsConstructor
class CreationFlightController {

    private final CreationFlightService service;

    @PostMapping
    ResponseEntity<?> createBy(@RequestBody @Valid FlightForm form, UriComponentsBuilder builder) {
        var code = service.createNewFlightBy(form);

        var uri = builder.path("/flights/{code}").build("<TODO>");

        return created(uri).build();
    }

    @Data
    static class FlightForm {
        private Long aircraftId;

        private Site departureSite;
        private LocalDateTime departureTime;

        private Site arrivalSite;
        private LocalDateTime arrivalTime;

    }
}
