package me.github.com.ifly.flights.features;

import lombok.AllArgsConstructor;
import me.github.com.ifly.flights.features.CreationFlightController.FlightForm;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
class CreationFlightService {


    private final FligthFormConverter converter;

    public Long createNewFlightBy(FlightForm form) {

        var flight = converter.converter(form);

        return flight.getId();
    }
}
