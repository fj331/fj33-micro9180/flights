package me.github.com.ifly.flights.infra;

public interface Mapper<S, T> {
    T converter(S source);
}
