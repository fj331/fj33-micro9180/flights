package me.github.com.ifly.flights.configurations;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBinding(Channels.class)
public class ChannelConfiguration {

}
