package me.github.com.ifly.flights.features;

import me.github.com.ifly.flights.domain.Site;
import me.github.com.ifly.flights.infra.ResourceNotFoundException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "airports", fallback = AirportClient.AirportClientFallback.class)
public interface AirportClient {

    @GetMapping("/airports/{airportId}/sites/{gateId}")
    Site existSiteBy(@PathVariable String airportId, @PathVariable String gateId);

    @Component
    class AirportClientFallback implements AirportClient {

        @Override
        public Site existSiteBy(String airportId, String gateId) {
            throw new ResourceNotFoundException("Cannot find site");
        }
    }
}
