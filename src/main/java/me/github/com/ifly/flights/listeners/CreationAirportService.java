package me.github.com.ifly.flights.listeners;

import org.springframework.stereotype.Service;

@Service
public class CreationAirportService {
    public void registerAirport(AirportListener.CreatedAirportEvent event) {
        System.out.println(event);
    }
}
