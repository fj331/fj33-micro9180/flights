package me.github.com.ifly.flights.listeners;

import me.github.com.ifly.flights.FlightsApplication;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.contract.stubrunner.StubTrigger;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;

import static me.github.com.ifly.flights.listeners.AirportListener.CreatedAirportEvent;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.then;

@SpringBootTest(classes = FlightsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.NONE)
@AutoConfigureStubRunner(ids = "me.github.fwfurtado.ifly:airports:+:stubs:9090", stubsMode = StubRunnerProperties.StubsMode.LOCAL)
class AirportListenerTest {

    @Autowired
    private StubTrigger trigger;

    @MockBean
    private CreationAirportService service;

    @Captor
    private ArgumentCaptor<CreatedAirportEvent> eventArgumentCaptor;

    @Test
    void shouldReceiveEvent() {
        trigger.trigger("create-airport");

        then(service).should().registerAirport(eventArgumentCaptor.capture());
        var event = eventArgumentCaptor.getValue();

        assertEquals("CGH", event.getIdentifier());
        assertEquals("SP", event.getLocation().get("state"));

    }
}