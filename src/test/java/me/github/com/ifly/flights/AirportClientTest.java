package me.github.com.ifly.flights;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.server.EnableStubRunnerServer;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@EnableStubRunnerServer
@AutoConfigureStubRunner(ids = "me.github.fwfurtado.ifly:airports:+:stubs:9090", stubsMode = StubRunnerProperties.StubsMode.LOCAL)
public class AirportClientTest {


    private RestTemplate rest = new RestTemplate();
    private Map<String, Object> payload = Map.of("gate", "D101");

    @Test
    void test() {

        rest.put("http://localhost:9090/airports/gru/gates", payload);
    }

    @Test
    void test2() {
        var exception = assertThrows(HttpClientErrorException.class, () -> rest.put("http://localhost:9090/airports/cgh/gates", payload));

       assertEquals(HttpStatus.NOT_FOUND, exception.getStatusCode());

    }

    @Test
    void test3() {
        var exception = assertThrows(HttpClientErrorException.class, () -> rest.put("http://localhost:9090/airports/cnf/gates", Map.of("gate", "")));

       assertEquals(HttpStatus.BAD_REQUEST, exception.getStatusCode());

    }
}
